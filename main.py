# -*- coding: utf-8 -*-

from min_cut import min_cut
from skimage import io, color
import cv2
import numpy as np
import random
import time
from os import walk
import os

# 路徑
savefile_path = "./Result/07/"
picture_path  = "./image/stone_13/"

depth  = 10  # 重疊 pixel
height = 2   # 輸出圖高
width  = 2   # 輸出圖長

# min cut 參數
overlap     = 0.3 # 重疊區域
filter_size = 20  # filter大小



# 全域變數
AllImage         = []
my_arr           = []
NextImg          = 0
mode             = 0
best_result_mode = 0
num              = ''
target           = ''
image_left       = ''
image_right_num  = ''


# 存放鏡像後的圖片
LRMirrorImage   = []
UDMirrorImage   = []
LRUDMirrorImage = []

# 列出資料夾中的所有圖片
def check_picture_path():
    if os.path.exists(picture_path):
        for root, dirs, files in walk(picture_path):
            files = sorted(files)
    else:
        print("請檢查路徑 !")
    return files, picture_path
    


# 檢查資料夾是否存在
def check_save_path():
    if not os.path.exists(savefile_path):
        os.makedirs(savefile_path)



# 儲存參數
def saveParams(best):
    #write
    text_file = open(savefile_path + 'params.txt', "a")
    text_file.write(best)
    text_file.write('\n')
    text_file.close()



# 讀圖
def load_image():
    files, picture_path = check_picture_path()
    Mylist = []
    for index in range(len(files)):
        locals()['original_img'+str(index)] = cv2.imread(picture_path + files[index])
    Mylist.append(locals())
    del Mylist[0]['index']
    del Mylist[0]['Mylist']
    del Mylist[0]['files']
    del Mylist[0]['picture_path']
    for key, value in Mylist[0].items():
        AllImage.append(value)



# 左右鏡像
def lrmirror():
    for index in range(len(AllImage)):
        LRMirrorImage.append(np.fliplr(AllImage[index]))


# 上下鏡像
def udmirror():
    for index in range(len(AllImage)):
        UDMirrorImage.append(np.flipud(AllImage[index]))


# 上下左右鏡像
def lrudmirror():
    for index in range(len(AllImage)):
        LRUDMirrorImage.append(np.flipud(LRMirrorImage[index]))



# 找相似的圖
def find_similar():
    global NextImg, image_right_num, target, mode
    image_status = "左圖: {}  右圖: {}  深度: {}".format(image_left.shape, NextImg, depth)
    print(image_status)
    img1_height, img1_width, a1 = image_left.shape         # 左圖
    img2_height, img2_width, a2 = target[NextImg].shape    # 右圖
    # 轉成LAB空間
    img1 = color.rgb2lab(image_left)
    img2 = color.rgb2lab(target[NextImg])
    # 找要切割的範圍
    L1 = img1[:,img1_width-depth:,0]
    L2 = img2[:,:depth,0]
    A1 = img1[:,img1_width-depth:,1]
    A2 = img2[:,:depth,1]
    B1 = img1[:,img1_width-depth:,2]
    B2 = img2[:,:depth,2]
    # 兩張照片LAB值相減
    L = L1 - L2
    A = A1 - A2
    B = B1 - B2
    # 兩張圖片LAB值相減後做最小平方法
    value = np.sqrt(np.square(L) + np.square(A) + np.square(B))
    # 切割後的圖片長寬大小
    height, width = value.shape
    # 切割後的圖片有幾個pixel
    pixel_sum = height * width
    # 計算平均值
    avg = value.sum() / pixel_sum
    my_arr.append([avg,NextImg,depth,mode])
    
    if mode != 4:
        NextImg += 1
        if NextImg < len(target):
            find_similar()
        else:
            NextImg = 0

            if mode == 0:
                lrmirror()
                target = LRMirrorImage
                mode = 1
                print("\n切換模式: 左右鏡像")
                time.sleep(5)
                NextImg = 0
                find_similar()
            elif mode == 1:
                udmirror()
                target = UDMirrorImage
                mode = 2
                print("\n切換模式: 上下鏡像")
                time.sleep(5)
                NextImg = 0
                find_similar()
            elif mode == 2:
                UDMirrorImage.clear()
                lrudmirror()
                LRMirrorImage.clear()
                target = LRUDMirrorImage
                mode = 3
                print("\n切換模式: 上下左右鏡像")
                time.sleep(5)
                NextImg = 0
                find_similar()
            else:
                print("\n正在進行篩選.....")
                find_avg_min = []
                for i in range(len(my_arr)):
                    find_avg_min.append(my_arr[i][0])
                best_result_image = my_arr[find_avg_min.index(min(find_avg_min))][1]
                best_result_depth = my_arr[find_avg_min.index(min(find_avg_min))][2]
                best_result_mode  = my_arr[find_avg_min.index(min(find_avg_min))][3]
                best = "最佳圖片: 第{}張, 深度:{}, 模式:{}".format(best_result_image, best_result_depth, best_result_mode)
                print(best)
                print(" ")
                saveParams(best)
                NextImg = 0
                target = AllImage
                image_right_num = best_result_image
                LRUDMirrorImage.clear()
                find_avg_min.clear()
                my_arr.clear()
                mode = 4



def main():
    global image_left, image_right_num, target, num, mode
    check_save_path()
    load_image()
    image_left = AllImage[random.randint(0,(len(AllImage)-1))]
    image = []
    target = AllImage
    for y in range(height):
        for x in range(width):
            print("第 {} 列，第 {} 張開始.....".format(y, x))
            find_similar()

            # 左右 min cut
            if best_result_mode == 0:
                image_right = AllImage[image_right_num]
            elif best_result_mode == 1:
                lrmirror()
                image_right = LRMirrorImage[image_right_num]
            elif best_result_mode == 2:
                udmirror()
                image_right = UDMirrorImage[image_right_num]
            elif best_result_mode == 3:
                lrudmirror()
                image_right = LRUDMirrorImage[image_right_num]
            im_up = min_cut(image_left, image_right, overlap, filter_size, 1)
            cv2.imwrite(savefile_path + "output_01.jpg", im_up, [cv2.IMWRITE_JPEG_QUALITY, 100])
            image_left = im_up  # 左圖更改為拼接後的圖
            LRMirrorImage.clear()
            UDMirrorImage.clear()
            LRUDMirrorImage.clear()
            mode = 0  # 恢復參數設定
        
        image.append(image_left)
        
        
        image_left = AllImage[random.randint(0,(len(AllImage)-1))]  # 恢復參數設定
        
        # 上下 min cut
        if len(image) >= 2:
            im_dn = min_cut(image[0], image[1], overlap, filter_size, 2)
            cv2.imwrite(savefile_path + "output_02.jpg", im_dn, [cv2.IMWRITE_JPEG_QUALITY, 100])
            image.clear()
            image.append(im_dn)



if __name__ == '__main__':
    main()
    


    
