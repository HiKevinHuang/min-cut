# Min cut


## 環境需求
* Python 3 (64位元)

## 安裝虛擬環境、套件
* 點擊 virtualenv.bat 來安裝並啟動虛擬環境。

![](https://i.imgur.com/MQCLrfC.jpg)


---

![](https://i.imgur.com/bs9Fgwj.jpg)


* cd 回去上上層，安裝套件
```
$ cd ..
$ cd ..
$ pip install -r requirements.txt
```

![](https://i.imgur.com/8iPV9VU.jpg)


## 參數設定
```
savefile_path : 輸出圖路徑
picture_path  : 圖片路徑
depth         : 重疊 pixel
width         : 結果圖的長
height        : 結果圖的寬
overlap       : 重疊區域    (min cut 參數)
filter_size   : filter大小 (min cut 參數)
```

## 執行專案
```
$ python main.py
```