import cv2
import numpy as np
import numpy.matlib


def im2double(im):
    min_val = np.min(im.ravel())
    max_val = np.max(im.ravel())
    out = (im.astype('float') - min_val) / (max_val - min_val)
    return out
    

def minDiffBoundary(im1, im2, mix, direction):
    h1, w1, ch1 = im1.shape
    h2, w2, ch2 = im2.shape
    if (h1 != h2) and (w1 != w2):
        print('error: two input images size mis-match')

    f = np.ones((7,7), np.float32) / 49 # filter

    if direction == 1:
        blk1 = im1[:, w1-mix:w1, :]    # 左圖重疊區域
        blk2 = im2[:, 0:mix, :]        # 右圖重疊區域

        # 模糊
        blk1 = cv2.filter2D(blk1, -1, f, cv2.BORDER_REFLECT_101)
        blk2 = cv2.filter2D(blk2, -1, f, cv2.BORDER_REFLECT_101)

        # 計算色差
        B1 = blk1[:,:,0]
        B2 = blk2[:,:,0]
        G1 = blk1[:,:,1]
        G2 = blk2[:,:,1]
        R1 = blk1[:,:,2]
        R2 = blk2[:,:,2]
        dE = np.sqrt((np.square(B1-B2) + np.square(G1-G2) + np.square(R1-R2)))

        base2 = np.linspace(0, 0.4, mix//2).astype('float')  # 在 0~0.4 之間產生等差數列
        base2 = base2.reshape(1, base2.shape[0])  # 橫的轉直的
        base1 = np.fliplr(base2)             # 左右鏡像
        base = cv2.hconcat([base1, base2])   # 水平拼接
        base = np.matlib.repmat(base, h1, 1) # 向右拉伸
        dE = dE + base # 影像色差加基本色差
        
        min_values = np.min(dE,axis=1) # 找出每列色差最小的值
        idx = np.argmin(dE, axis=1).astype('float') # 按每列求出最小值的索引
        f1 = np.ones((1,11), np.float32)/11  # Kernal
        idx = cv2.filter2D(idx, -1, f1, cv2.BORDER_REFLECT_101) # 曲線平滑化
        idx = np.round(idx).astype('int')    # 四捨五入
        
        dE = np.expand_dims(dE, axis=2)
        dE_map = np.concatenate((dE, dE, dE), axis=-1) # 合併成三通道
        mask = np.zeros([h1, mix])  # 空白 mask
        
        for x in range(h1):
            dE_map[x][int(idx[x])][1] = 255
            mask[x, 0:int(idx[x])] = 1
        
        
    if  direction == 2:
        blk1 = im1[h1-mix:h1,:,:]
        blk2 = im2[0:mix,:,:]

        # 模糊
        blk1 = cv2.filter2D(blk1, -1, f, cv2.BORDER_REFLECT_101)
        blk2 = cv2.filter2D(blk2, -1, f, cv2.BORDER_REFLECT_101)

        # 計算色差
        B1 = blk1[:,:,0]
        B2 = blk2[:,:,0]
        G1 = blk1[:,:,1]
        G2 = blk2[:,:,1]
        R1 = blk1[:,:,2]
        R2 = blk2[:,:,2]
        dE = np.sqrt((np.square(B1-B2) + np.square(G1-G2) + np.square(R1-R2)))

        base2 = np.linspace(0, 0.4, mix//2).astype('float')  # 在 0~0.4 之間產生等差數列
        base1 = np.flipud(base2)             # 上下鏡像
        base = cv2.vconcat([base1, base2])   # 垂直拼接
        base = np.matlib.repmat(base, 1, w1) # 向下拉伸
        dE = dE + base # 影像色差加基本色差
        min_values = np.min(dE,axis=0)  # 找出每行色差最小的值
        idx = np.argmin(dE, axis=0).astype('float') # 按每行求出最小值的索引
        f1 = np.ones((1,11), np.float32)/11  # Kernal
        idx = cv2.filter2D(idx, -1, f1, cv2.BORDER_REFLECT_101) # 曲線平滑化
        idx = np.round(idx).astype('int')    # 四捨五入
        
        # 合併成三通道
        dE = np.expand_dims(dE, axis=2)
        dE_map = np.concatenate((dE, dE, dE), axis=-1)

        mask = np.zeros([mix, w1])  # 空白 mask
        
        # 繪製路徑
        for x in range(w1):
            dE_map[int(idx[x])][x][1] = 255
            mask[0:int(idx[x]), x] = 1
        
    return mask


def min_cut(pathA, pathB, overlap, filter_size, direction):
    # 讀圖
#    imageA = cv2.imread(pathA)
#    imageB = cv2.imread(pathB)
    imageA = pathA
    imageB = pathB

    # 轉成浮點數 (0~1)
    imA = im2double(imageA)
    imB = im2double(imageB)
    h, w, c = imageA.shape  # 圖片高、寬、通道
    mix = 2 * round(overlap * h / 2)  # 找出兩圖間要重疊的起始點
    f = np.ones((filter_size, filter_size)) / np.square(filter_size)
    
    Result = 0

    if direction == 1:
        mask_up = minDiffBoundary(imA, imB, mix, direction)
        mask_up = cv2.filter2D(mask_up, -1, f, cv2.BORDER_REFLECT_101)
        
        # 創建重疊區域空白圖，並轉為三通道
        mix_up = np.zeros(mask_up.shape)
        mix_up = np.expand_dims(mix_up, axis=2)
        mix_up = np.concatenate((mix_up, mix_up, mix_up), axis=-1)
        
        # 左圖 + 右圖融合
        for ch in range(3):
            mix_up[:,:,ch] = (mask_up * imA[:, w-mix:w, ch]) + ((1-mask_up) * imB[:, 0:mix, ch])
            
        # 水平並排
        im_up = cv2.hconcat([imA[:,0:w-mix,:], mix_up])
        im_up = cv2.hconcat([im_up, imB[:,mix:w,:]])
        
        # 還原成 0~ 255
        im_up = im_up * 255
        Result = im_up

    elif direction == 2:
        mask_dn = minDiffBoundary(imA, imB, mix, direction)
        mask_dn = cv2.filter2D(mask_dn, -1, f, cv2.BORDER_REFLECT_101)
        
        # 創建重疊區域空白圖，並轉為三通道
        mix_dn = np.zeros(mask_dn.shape)
        mix_dn = np.expand_dims(mix_dn, axis=2)
        mix_dn = np.concatenate((mix_dn, mix_dn, mix_dn), axis=-1)
        
        # 上圖 + 下圖融合
        for ch in range(3):
            mix_dn[:,:,ch] = (mask_dn * imA[h-mix:h, :, ch]) + ((1-mask_dn) * imB[0:mix, :, ch])
        
        # 垂直並排
        im_dn = cv2.vconcat([imA[0:h-mix,:,:], mix_dn])
        im_dn = cv2.vconcat([im_dn, imB[mix:w,:,:]])
        
        # 還原成 0~ 255
        im_dn = im_dn * 255
        Result = im_dn
        
    return Result